package com.microservices.transactions.service;

import java.util.List;

import com.microservices.transactions.model.Transaction;

public interface TransactionService {

	public Transaction getTransactionDetail(long transactionId);
	
	public List<Transaction> getTransactions(String accountId);
}
